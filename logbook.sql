-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 04, 2017 at 02:27 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `lb_admin`
--

CREATE TABLE `lb_admin` (
  `id_user` int(11) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_admin`
--

INSERT INTO `lb_admin` (`id_user`, `owner`, `username`, `password`) VALUES
(1, 'Admin 1', 'admin', 'azsxdcfv'),
(2, 'Admin 2', 'admin2', 'azsxdcfv'),
(3, 'Superadmin', 'superadmin', 'azsxdcfv');

-- --------------------------------------------------------

--
-- Table structure for table `lb_log_content`
--

CREATE TABLE `lb_log_content` (
  `id_log_content` int(11) NOT NULL,
  `log_content` text NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `nim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_log_content`
--

INSERT INTO `lb_log_content` (`id_log_content`, `log_content`, `created_at`, `nim`) VALUES
(1, 'asdasdasdasd', '20 Sep 2017', 15010086),
(2, 'Belajar ionic bagian http get', '20 Sep 2017', 15010086),
(3, '', '20 Sep 2017', 15010086),
(4, 'Membantu menyelesaikan aplikasi mobile', '20 Sep 2017', 15010086),
(5, 'tidak ada', '20 Sep 2017', 15010086),
(6, 'nothing', '20 Sep 2017', 15010086),
(7, 'Membantu membuat flowchart untuk aplikasi selanjutnya.', '20 Sep 2017', 15010086),
(8, 'Memperbaiki laptop rusak', '22 Sep 2017', 15010086),
(9, 'Tidak ada', '28 Sep 2017', 12345678),
(10, 'Membuat flowchart', '28 Sep 2017', 12345678),
(11, 'Tidak ada apa apa', '29 Sep 2017', 15010086),
(12, 'Nothing, I do nothing today', '02 Oct 2017', 98765432);

-- --------------------------------------------------------

--
-- Table structure for table `lb_user`
--

CREATE TABLE `lb_user` (
  `id_user` int(11) NOT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  `password` text,
  `created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_user`
--

INSERT INTO `lb_user` (`id_user`, `owner`, `nim`, `password`, `created_at`) VALUES
(1, 'Muhammad Rizqi Alfaruq', 15010086, 'azsxdcfv', '19 September 2017'),
(2, 'Rony Ahmadi', 12345678, 'azsxdcfv', '19 September 2017'),
(4, 'Sony PB', 12345672, 'azsxdcfv', '19 Sep 2017'),
(7, 'Karlitos', 98765432, 'azsxdcfv', '02 Oct 2017');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lb_admin`
--
ALTER TABLE `lb_admin`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `lb_log_content`
--
ALTER TABLE `lb_log_content`
  ADD PRIMARY KEY (`id_log_content`);

--
-- Indexes for table `lb_user`
--
ALTER TABLE `lb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lb_admin`
--
ALTER TABLE `lb_admin`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lb_log_content`
--
ALTER TABLE `lb_log_content`
  MODIFY `id_log_content` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `lb_user`
--
ALTER TABLE `lb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
