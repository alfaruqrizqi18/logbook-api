<div class="main-content">

	<div class="row">

		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">

			<ul class="user-info pull-left pull-none-xsm">

				<!-- Profile Info -->
				<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/thumb-1@2x.png" alt="" class="img-circle" width="44" />
						<?php echo $this->session->userdata('owner'); ?>
					</a>
		</div>



	</div>

	<hr />
	<h2>Data Admin</h2>

	<br />

	<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table1 = jQuery( '#table-1' );

		// Initialize DataTable
		$table1.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});

		// Initalize Select Dropdown after DataTables is created
		$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});
	} );
	</script>
	<a href="admin/tambah_user_admin"><button type="button" class="btn btn-success">Tambah Data Baru</button><br><br></a>
	<table class="table table-bordered datatable" id="table-1">
		<thead>
			<tr>
				<th data-hide="phone">ID User</th>
				<th>Owner</th>
				<th data-hide="phone">Username</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($admin as $data): ?>
				<tr class="odd gradeX">
					<td><?php echo $data['id_user']; ?></td>
					<td><?php echo $data['owner']; ?></td>
					<td><?php echo $data['username']; ?></td>
				</tr>
			<?php endforeach; ?>

		</tbody>
		<tfoot>
			<tr>
				<th>ID User</th>
				<th>Owner</th>
				<th>Username</th>
			</tr>
		</tfoot>
	</table>
</div>
</div>





<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/datatables/datatables.css">
<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="assets/js/gsap/TweenMax.min.js"></script>
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/datatables/datatables.js"></script>
<script src="assets/js/select2/select2.min.js"></script>
<script src="assets/js/neon-chat.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="assets/js/neon-demo.js"></script>

</body>
</html>
