<div class="main-content">
  <div class="row">
  			<div class="col-md-12">

  				<div class="panel panel-primary" data-collapsed="0">

  					<div class="panel-heading">
  						<div class="panel-title">
  							Default Form Inputs
  						</div>

  						<div class="panel-options">
  							<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
  							<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
  							<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
  							<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
  						</div>
  					</div>

  					<div class="panel-body">

  						<form role="form" method="POST" class="form-horizontal form-groups-bordered">

  							<div class="form-group">
  								<label for="field-1" class="col-sm-3 control-label">Owner</label>

  								<div class="col-sm-5">
  									<input type="text" name="owner" class="form-control" id="field-1" placeholder="........." required>
  								</div>
  							</div>

                <div class="form-group">
  								<label for="field-1" class="col-sm-3 control-label">Username</label>

  								<div class="col-sm-5">
  									<input type="number" name="username" class="form-control" id="field-2" placeholder="........." required>
  								</div>
  							</div>

  							<div class="form-group">
  								<label for="field-3" class="col-sm-3 control-label">Password</label>

  								<div class="col-sm-5">
  									<input type="password" name="password" class="form-control" id="field-3" placeholder="........." required>
  								</div>
  							</div>

  							<div class="form-group">
  								<div class="col-sm-offset-3 col-sm-5">
  									<button type="submit" class="btn btn-default">Tambahkan</button>
  								</div>
  							</div>
  						</form>

  					</div>

  				</div>

  			</div>
  		</div>
</div>

</div>




<!-- Bottom scripts (common) -->
<script src="assets/js/gsap/TweenMax.min.js"></script>
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/bootstrap-switch.min.js"></script>
<script src="assets/js/neon-chat.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="assets/js/neon-demo.js"></script>

</body>
</html>
