<?php
 
require APPPATH . '/libraries/REST_Controller.php';
 
class User extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
    }
 
    // show data mahasiswa
    function index_get() {
        $nim = $this->get('nim');
        $password = $this->get('password');
        if ($nim == ''){
            $this->db->order_by('id_user', 'DESC');
            $query = $this->db->get('lb_user')->result();

        }elseif ($nim) {
            $this->db->where('nim', $nim);
            $query = $this->db->get('lb_user')->result();
        }
        $this->response($query, 200);
    }
 
    // insert new data to mahasiswa
    function index_post() {
        $data = array(
                    'owner'           => $this->post('owner'),
                    'nim'          => $this->post('nim'),
                    'password'        => $this->post('password'),
                    'created_at' => date('d M Y'));
        $insert = $this->db->insert('lb_user', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // update data mahasiswa
    function index_put() {
        $nim = $this->put('id_user');
        $data = array(
                    'owner'           => $this->post('owner'),
                    'nim'          => $this->post('nim'),
                    'password'        => $this->post('password'));
        $this->db->where('nim', $nim);
        $update = $this->db->update('lb_user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // delete mahasiswa
    function index_delete() {
        $nim = $this->delete('nim');
        $this->db->where('nim', $nim);
        $delete = $this->db->delete('lb_user');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
}