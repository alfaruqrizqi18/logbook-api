<?php
/**
 *
 */
class Admin extends CI_Controller {

  function __construct(){
		parent::__construct();
    # code...
  }

  function index(){
    if ($this->session->userdata('id_user') != null || $this->session->userdata('id_user') != "") {
      redirect('admin/data_admin');
    }else{
      $this->load->view('admin/login');
    }
  }

  function data_admin(){
    $query = $this->db->query(
              "SELECT *
               FROM lb_admin")->result_array();
    $data['admin'] = $query;
    $this->load->view('admin/header');
    $this->load->view('admin/data-admin', $data);
  }

  function data_mahasiswa(){
    $query = $this->db->query(
              "SELECT *
               FROM lb_user")->result_array();
    $data['mahasiswa'] = $query;
    $this->load->view('admin/header');
    $this->load->view('admin/data-mhs', $data);
  }

  function tambah_user_mahasiswa(){
    if ($this->input->server('REQUEST_METHOD') == 'POST'){
      # code...
      $owner = $_POST['owner'];
      $username = $_POST['username'];
      $password = $_POST['password'];
          $data = array(
            'owner' => $owner,
            'nim' => $username,
            'password' => $password,
            'created_at' => date('d M Y'));
          $this->db->insert('lb_user', $data);
      redirect('admin/data_mahasiswa');
    }else{
      $this->load->view('admin/header');
      $this->load->view('admin/add-user');
    }

  }

  function tambah_user_admin(){
    if ($this->input->server('REQUEST_METHOD') == 'POST'){
      # code...
      $owner = $_POST['owner'];
      $username = $_POST['username'];
      $password = $_POST['password'];
      $data = array(
        'owner' => $owner,
        'username' => $username,
        'password' => $password);
      $this->db->insert('lb_admin', $data);
      redirect('admin/data_admin');
    }else{
      $this->load->view('admin/header');
      $this->load->view('admin/add-admin');
    }

  }

  function login(){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $query    = $this->db->query(
                "SELECT *
                 FROM lb_admin
                 WHERE BINARY username = '$username'
                 AND BINARY password = '$password'
                "
                );
    if ($query->num_rows() == 1) {
      foreach ($query->result() as $data) {
        $session['id_user'] = $data->id_user;
        $session['owner'] = $data->owner;
        $session['username'] = $data->username;
        $this->session->set_userdata($session);
      }
    }
    redirect('admin');
  }

  function logout(){
    $this->session->unset_userdata('id_user');
    $this->session->unset_userdata('owner');
    $this->session->unset_userdata('username');
    session_destroy();
    redirect(base_url('admin'));
  }

}
 ?>
