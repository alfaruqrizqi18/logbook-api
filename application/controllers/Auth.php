<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller {

  function __construct($config = 'rest') {
    parent::__construct($config);
  }


    // insert new data to mahasiswa
  function index_post() {
   $data = array(
    'nim' => $this->post('nim'),
    'password' => $this->post('password'));
    $query = $this->db->get_where('lb_user', 
      array('nim' => $data['nim'],
                'password' => $data['password']))->result();
    $result = '';
    if (count($query) == 1) {
      foreach ($query as $data_result) {
        $result = array(
          'nim' => $data_result->nim,
          'password' => $data_result->password,
          'owner' => $data_result->owner,
          'status' => 'success' );
      }
    }else{
      $result = 'failed';
    }
    
    $this->response($result);
}

}