<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
require APPPATH . '/libraries/REST_Controller.php';
 
class Log extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
    }
 
    // show data mahasiswa
    function index_get() {
        $id = $this->get('id');
        $nim = $this->get('nim');
        if ($nim){
            $this->db->where('nim', $nim);
            $this->db->order_by('id_log_content', 'DESC');
            $query = $this->db->get('lb_log_content')->result();
        }else if ($id){
            $this->db->where('id_log_content', $id);
            $query = $this->db->get('lb_log_content')->result();
        }else if(!$nim){
            $this->db->order_by('id_log_content', 'DESC');
            $query = $this->db->get('lb_log_content')->result();
        }else if(!$id){
            $this->db->order_by('id_log_content', 'DESC');
            $query = $this->db->get('lb_log_content')->result();
        }
        if ($query) {
            $this->response($query, 200);
        }else{
            $this->response(array());
        }
    }
 
    // insert new data to mahasiswa
    function index_post() {
        $data = array(
                    'log_content'	=> $this->post('log_content'),
                    'nim'	=> $this->post('nim'),
                    'created_at' => date('d M Y'));
        $insert = $this->db->insert('lb_log_content', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // update data mahasiswa
    // function index_put() {
    //     $nim = $this->put('id_user');
    //     $data = array(
    //                 'owner'           => $this->post('owner'),
    //                 'nim'          => $this->post('nim'),
    //                 'username'    => $this->post('username'),
    //                 'password'        => $this->post('password'));
    //     $this->db->where('nim', $nim);
    //     $update = $this->db->update('lb_user', $data);
    //     if ($update) {
    //         $this->response($data, 200);
    //     } else {
    //         $this->response(array('status' => 'fail', 502));
    //     }
    // }
 
    // // delete mahasiswa
    // function index_delete() {
    //     $nim = $this->delete('nim');
    //     $this->db->where('nim', $nim);
    //     $delete = $this->db->delete('lb_user');
    //     if ($delete) {
    //         $this->response(array('status' => 'success'), 201);
    //     } else {
    //         $this->response(array('status' => 'fail', 502));
    //     }
    // }
 
}